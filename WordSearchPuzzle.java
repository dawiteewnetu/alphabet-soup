import java.util.List;

public class WordSearchPuzzle {
        int rows;
        int cols;
        char[][] grid;
        List<String> wordsToFind;
        
        WordSearchPuzzle(int rows, int cols, char[][] grid, List<String> wordsToFind) {
            this.rows = rows;
            this.cols = cols;
            this.grid = grid;
            this.wordsToFind = wordsToFind;
        }
    }