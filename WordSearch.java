import java.io.*;
import java.util.*;

public class WordSearch {

    public static WordSearchPuzzle readInputFile(String filename) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(
            WordSearch.class.getClassLoader().getResourceAsStream(filename)
        ));
        String[] dimensions = reader.readLine().trim().split("x");
        int rows = Integer.parseInt(dimensions[0]);
        int cols = Integer.parseInt(dimensions[1]);

        char[][] grid = new char[rows][cols];
        for (int i = 0; i < rows; i++) {
            String[] line = reader.readLine().trim().split(" ");
            for (int j = 0; j < cols; j++) {
                grid[i][j] = line[j].charAt(0);
            }
        }

        List<String> wordsToFind = new ArrayList<>();
        String word;
        while ((word = reader.readLine()) != null) {
            wordsToFind.add(word.trim());
        }
        reader.close();

        return new WordSearchPuzzle(rows, cols, grid, wordsToFind);
    }

    public static String searchWordInGrid(char[][] grid, String word) {
        int rows = grid.length;
        int cols = grid[0].length;
        int[][] directions = {
            {-1, -1}, {-1, 0}, {-1, 1}, 
            {0, -1}, {0, 1}, 
            {1, -1}, {1, 0}, {1, 1}
        };

        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                for (int[] dir : directions) {
                    String result = searchDirection(grid, word, x, y, dir[0], dir[1]);
                    if (result != null) {
                        return word + " " + result;
                    }
                }
            }
        }
        return word + " Not Found";
    }

    private static String searchDirection(char[][] grid, String word, int startX, int startY, int dx, int dy) {
        int rows = grid.length;
        int cols = grid[0].length;
        int x = startX, y = startY;

        for (int i = 0; i < word.length(); i++) {
            if (x < 0 || x >= rows || y < 0 || y >= cols || grid[x][y] != word.charAt(i)) {
                return null;
            }
            x += dx;
            y += dy;
        }

        x -= dx;
        y -= dy;
        return startX + ":" + startY + " " + x + ":" + y;
    }

    public static void main(String[] args) {
        String inputFilename = "input.txt";
        try {
            WordSearchPuzzle puzzle = readInputFile(inputFilename);
            for (String word : puzzle.wordsToFind) {
                String result = searchWordInGrid(puzzle.grid, word);
                System.out.println(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}